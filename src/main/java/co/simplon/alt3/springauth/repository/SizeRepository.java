package co.simplon.alt3.springauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.alt3.springauth.entity.Size;


public interface SizeRepository extends JpaRepository<Size, Integer> {
  
}
