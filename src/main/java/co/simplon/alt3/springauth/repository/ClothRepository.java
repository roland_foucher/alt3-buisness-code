package co.simplon.alt3.springauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.alt3.springauth.entity.Cloth;

public interface ClothRepository extends JpaRepository<Cloth, Integer> {
  
}
