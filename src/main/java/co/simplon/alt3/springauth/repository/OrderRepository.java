package co.simplon.alt3.springauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.alt3.springauth.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {
  
}
