package co.simplon.alt3.springauth.exception;

public class UserExistsException extends Exception {

  public UserExistsException() {
    super("User already Exist");
  }

}
