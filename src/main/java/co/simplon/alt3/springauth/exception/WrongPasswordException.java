package co.simplon.alt3.springauth.exception;

public class WrongPasswordException extends Exception {

  public WrongPasswordException() {
    super("passwod didn't match");
  }

}
