package co.simplon.alt3.springauth.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt3.springauth.auth.UserRoles;
import co.simplon.alt3.springauth.entity.User;
import co.simplon.alt3.springauth.repository.UserRepository;

@Service
public class AdminService {

  @Autowired
  UserRepository userRepository;

  public void changeRole(
      Integer id,
      UserRoles role) {
    User user = userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    user.setRole(role.toString());
    userRepository.save(user);
  }
}
