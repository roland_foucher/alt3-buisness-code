package co.simplon.alt3.springauth.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt3.springauth.auth.ChangePasswordDto;
import co.simplon.alt3.springauth.entity.Cloth;
import co.simplon.alt3.springauth.entity.Order;
import co.simplon.alt3.springauth.entity.OrderItem;
import co.simplon.alt3.springauth.entity.Size;
import co.simplon.alt3.springauth.entity.User;
import co.simplon.alt3.springauth.enums.Status;
import co.simplon.alt3.springauth.exception.NotEnougthStockException;
import co.simplon.alt3.springauth.exception.UserExistsException;
import co.simplon.alt3.springauth.exception.WrongPasswordException;
import co.simplon.alt3.springauth.repository.OrderRepository;
import co.simplon.alt3.springauth.repository.UserRepository;

@Service
public class UserService {

  @Autowired
  UserRepository userRepository;

  @Autowired
  OrderRepository orderRepository;

  @Autowired
  private PasswordEncoder encoder;

  public String test(
      User user) {

    if (user != null) {
      return "Hello " + user.getEmail();
    }
    return "not connected";
  }

  public User register(User user) throws UserExistsException {
    if (userRepository.findByEmail(user.getEmail()).isPresent()) {
      throw new UserExistsException();
    }
    user.setId(null);
    user.setRole("ROLE_USER");

    hashPassword(user);
    userRepository.save(user);

    // Optionnel, mais avec ça, on peut connecter le User automatiquement lors de
    // son inscription
    SecurityContextHolder.getContext()
        .setAuthentication(new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));

    return user;
  }

  private void hashPassword(User user) {
    String hashed = encoder.encode(user.getPassword());
    user.setPassword(hashed);
  }

  public void changePassowrd(ChangePasswordDto body, User user) throws WrongPasswordException {
    if (!encoder.matches(body.oldPassword, user.getPassword())) {
      throw new WrongPasswordException();
    }
    user.setPassword(body.newPassword);
    hashPassword(user);
    userRepository.save(user);
  }

  public List<Order> getCart(User user) {

    return user.getOrders()
        .stream()
        .filter(el -> el.getStatus() == Status.CART)
        .collect(Collectors.toList());
  }

  public void addItemToCart(Size size, Order order) throws NotEnougthStockException {

    if (size.getStock()<1) {
      throw new NotEnougthStockException();
    }
    
    order.getOrderItems()
      .stream()
      .filter(el -> el.getCloth() == size.getCloth() && el.getSize().getSizeLabel() == size.getSizeLabel())
      .findFirst()
      .ifPresentOrElse(
        el -> el.addOne(), 
        () -> order.getOrderItems().add(new OrderItem(1, size.getCloth().getPrice(), order, size.getCloth(), size)));

    orderRepository.save(order);
      
  }
}
