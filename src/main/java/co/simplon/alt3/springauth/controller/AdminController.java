package co.simplon.alt3.springauth.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt3.springauth.auth.UserRoles;
import co.simplon.alt3.springauth.entity.User;
import co.simplon.alt3.springauth.repository.UserRepository;
import co.simplon.alt3.springauth.services.AdminService;
import co.simplon.alt3.springauth.services.UserService;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user")
    public List<User> all() {
        
        return userRepository.findAll();            
    
        // throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Vous devez être connecté pour accéder à cette ressource");
    }

    @PatchMapping("/user/{id}/role/{role}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changeRole(@PathVariable int id, @PathVariable UserRoles role) {
        adminService.changeRole(id, role);
    }
}
