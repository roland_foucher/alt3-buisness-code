package co.simplon.alt3.springauth.auth;

public enum UserRoles {
    ROLE_USER,
    ROLE_ADMIN
}
