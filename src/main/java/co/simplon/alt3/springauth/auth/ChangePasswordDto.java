package co.simplon.alt3.springauth.auth;

public class ChangePasswordDto {
    public String oldPassword;
    public String newPassword;
}
