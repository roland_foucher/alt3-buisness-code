package co.simplon.alt3.springauth.enums;

public enum Status {
  CART, ORDERED, SENT, DELIVERED
}
