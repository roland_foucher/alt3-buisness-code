package co.simplon.alt3.springauth.enums;

public enum SizeLabel {
  XXS,XS,S,M,L,XL,XXL
}
