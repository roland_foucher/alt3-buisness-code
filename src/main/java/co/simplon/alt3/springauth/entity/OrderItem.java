package co.simplon.alt3.springauth.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class OrderItem {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private int quantity;
  private double price;

  @ManyToOne
  private Order order;

  @ManyToOne
  private Cloth cloth;

  @ManyToOne
  private Size size;
  
  public OrderItem(int quantity, double price, Order order, Cloth cloth, Size size) {
    this.quantity = quantity;
    this.price = price;
    this.order = order;
    this.cloth = cloth;
    this.size = size;
  }
  public OrderItem(
      Integer id,
      int quantity,
      double price) {
    this.id = id;
    this.quantity = quantity;
    this.price = price;
  }
  public OrderItem() {
  }
  public Integer getId() {
    return id;
  }
  public void setId(
      Integer id) {
    this.id = id;
  }
  public int getQuantity() {
    return quantity;
  }
  public void setQuantity(
      int quantity) {
    this.quantity = quantity;
  }
  public double getPrice() {
    return price;
  }
  public void setPrice(
      double price) {
    this.price = price;
  }
  public Order getOrder() {
    return order;
  }
  public void setOrder(
      Order order) {
    this.order = order;
  }
  public Cloth getCloth() {
    return cloth;
  }
  public void setCloth(
      Cloth cloth) {
    this.cloth = cloth;
  }
  public Size getSize() {
    return size;
  }
  public void setSize(
      Size size) {
    this.size = size;
  }

  public void addOne(){
    quantity++;
  }
}
