package co.simplon.alt3.springauth.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Cloth {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String label;
  private String fabrik;
  private Double price;

  @OneToMany(mappedBy = "cloth")
  List<OrderItem> orderItems = new ArrayList<>();

  @OneToMany(mappedBy = "cloth")
  List<Size> sizes = new ArrayList<>();

  public Cloth(
      Integer id,
      String label,
      String fabrik,
      Double price) {
    this.id = id;
    this.label = label;
    this.fabrik = fabrik;
    this.price = price;
  }

  public Cloth() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(
      Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(
      String label) {
    this.label = label;
  }

  public String getFabrik() {
    return fabrik;
  }

  public void setFabrik(
      String fabrik) {
    this.fabrik = fabrik;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(
      Double price) {
    this.price = price;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public List<Size> getSizes() {
    return sizes;
  }
}
