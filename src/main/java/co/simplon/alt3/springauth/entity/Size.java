package co.simplon.alt3.springauth.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import co.simplon.alt3.springauth.enums.SizeLabel;

@Entity
public class Size {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Enumerated(EnumType.STRING)
  private SizeLabel sizeLabel;

  private int stock;

  @ManyToOne
  private Cloth cloth;

  @OneToMany(mappedBy = "size")
  private List<OrderItem> orderItems = new ArrayList<>();

  public Size(
      Integer id,
      SizeLabel sizeLabel,
      int stock) {
    this.id = id;
    this.sizeLabel = sizeLabel;
    this.stock = stock;
  }

  public Size() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(
      Integer id) {
    this.id = id;
  }

  public SizeLabel getSizeLabel() {
    return sizeLabel;
  }

  public void setSizeLabel(
      SizeLabel sizeLabel) {
    this.sizeLabel = sizeLabel;
  }

  public int getStock() {
    return stock;
  }

  public void setStock(
      int stock) {
    this.stock = stock;
  }

  public Cloth getCloth() {
    return cloth;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public void setCloth(
      Cloth cloth) {
    this.cloth = cloth;
  }
}
