package co.simplon.alt3.springauth.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import co.simplon.alt3.springauth.enums.Status;

@Table(name = "order_table")
@Entity
public class Order {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Enumerated(EnumType.STRING)
  private Status status;

  private LocalDate localDate;

  @ManyToOne
  private User user;

  @OneToMany(mappedBy = "order")
  private List<OrderItem> orderItems = new ArrayList<>();

  public Order(
      Integer id,
      Status status,
      LocalDate localDate) {
    this.id = id;
    this.status = status;
    this.localDate = localDate;
  }

  public Order() {
  }

  public Integer getId() {
    return id;
  }

  public Status getStatus() {
    return status;
  }

  public LocalDate getLocalDate() {
    return localDate;
  }

  public Double getTotal() {
    return null;
  }

  public void setId(
      Integer id) {
    this.id = id;
  }

  public void setStatus(
      Status status) {
    this.status = status;
  }

  public void setLocalDate(
      LocalDate localDate) {
    this.localDate = localDate;
  }

  public User getUser() {
    return user;
  }

  public void setUser(
      User user) {
    this.user = user;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }
}
